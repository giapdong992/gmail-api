const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const gmail = google.gmail('v1');

const request = require('request');


// If modifying these scopes, delete token.json.
var SCOPES = [
  'https://mail.google.com/',
  'https://www.googleapis.com/auth/gmail.modify',
  'https://www.googleapis.com/auth/gmail.compose',
  'https://www.googleapis.com/auth/gmail.send'
];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';
//---------------------------------------\Modify/-----------------------------------------
//---------------------------------------\Modify/-----------------------------------------
//---------------------------------------\Modify/-----------------------------------------

var clientID = '538369601541-qfhel7g2a1n4iaj3kaft7uuttjoc452o.apps.googleusercontent.com';
var secretID = 'UncVsEUUGYCaUl-f2FzixBa6';
var refreshID = '1//0e4uRc2mNbPLLCgYIARAAGA4SNwF-L9IrVkbc60xgcGmM6a7HWZesW30ko6cOB_0ooyJngZLGyUJsfH_kusaUbWHq8aD5zLIIuvQ';

//---------------------------------------\Execute/-----------------------------------------
//---------------------------------------\Execute/-----------------------------------------
//---------------------------------------\Execute/-----------------------------------------

fs.readFile('data.json', (err, content) => {
  if (err) return console.log('Error loading client secret file:', err);
  // Authorize a client with credentials, then call the Gmail API.
  authorize(JSON.parse(content), listLabels);
});



//---------------------------------------\Function/--------------------------------------
//---------------------------------------\Function/--------------------------------------
//---------------------------------------\Function/--------------------------------------

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */

function authorize(credentials, callback) {
  const {client_secret, client_id, redirect_uris} = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) console.log(err);
    return getNewToken(oAuth2Client, callback);
    //oAuth2Client.setCredentials(JSON.parse(token));
    //callback(oAuth2Client);
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
    var dataString = `client_id=${clientID}&client_secret=${secretID}&refresh_token=${refreshID}&grant_type=refresh_token`;
    var header = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };
    options = {
        url: 'https://accounts.google.com/o/oauth2/token',
        headers: header,
        method: 'POST',
        body: dataString,
    };

    function execute(error, response, body) {
        if(error) console.log(error);
        if (!error && response.statusCode == 200) {
            var json = JSON.parse(body);
            oAuth2Client.setCredentials(json);

            fs.writeFile(TOKEN_PATH, JSON.stringify(json), (err) => {
                if (err) return console.error(err);
                console.log('Token stored to', TOKEN_PATH);
            });

            callback(oAuth2Client);
        }
        
    }
    request(options, execute);
}

/**
 * Lists the labels in the user's account.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function listLabels(auth) {
  const gmail = google.gmail({version: 'v1', auth});
  gmail.users.labels.list({
    userId: 'me',
  }, (err, res) => {
    if (err) return console.log('The API returned an error: ' + err);
    const labels = res.data.labels;
    if (labels.length) {
      console.log('Labels:');
      labels.forEach((label) => {
        console.log(`- ${label.name}`);
      });
    } else {
      console.log('No labels found.');
    }
  });
}

/**
 * Get the recent email from your Gmail account
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function getRecentEmail(auth) {
  // Only get the recent email - 'maxResults' parameter
  gmail.users.messages.list({auth: auth, userId: 'me', maxResults: 1,}, function(err, response) {
      if (err) {
          console.log('The API returned an error: ' + err);
          return;
      }

    // Get the message id which we will need to retreive tha actual message next.
    var message_id = response['data']['messages'][0]['id'];

    // Retreive the actual message using the message id
    gmail.users.messages.get({auth: auth, userId: 'me', 'id': message_id}, function(err, response) {
        if (err) {
            console.log('The API returned an error: ' + err);
            return;
        }

       console.log(response['data']);
    });
  });
}


/**
 * Get the recent email from your Gmail account
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function loadProfile(auth) {
  // Only get the recent email - 'maxResults' parameter
  gmail.users.getProfile({auth: auth, userId: 'me', maxResults: 1,}, function(err, response) {
      if (err) {
          console.log('The API returned an error: ' + err);
          return;
      }
      console.log(response);
  });
}
/**
 * 
 * 
 * {
  "access_token": "ya29.Il-9ByyxGh7ecAuDfipydx67oCaBpbLiaj_REMADN9sN5vMVPA_sJYnBemJVm4XHFrVcabDvN0yGdm5T_Mv5qL8ASxMA4KbFw8U5CiqawEDfYbdFix_DQ0LNMl3zt7gumg",
  "expires_in": 3599,
  "refresh_token": "1//0dQ4K5SCxDNe5CgYIARAAGA0SNwF-L9IrxE2DPYUkIqpcGmNfvIP55bY45jBG06ZEkAQc-xibijVbNFoNkBzJ6DwAxca4efp37PY",
  "scope": "https://mail.google.com/ https://www.googleapis.com/auth/gmail.send https://www.googleapis.com/auth/gmail.compose https://www.googleapis.com/auth/gmail.modify",
  "token_type": "Bearer"
}
 */